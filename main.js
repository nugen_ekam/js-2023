let credentials = {
  email: "",
  password: "",
};
let emailError = document.getElementById("email-error");
let passwordError = document.getElementById("password-error");

function handleLogin() {
  if (credentials.email === "") {
    emailError.innerHTML = "Email is required";
  } else if (credentials.password === "") {
    emailError.innerHTML = "";
    passwordError.innerHTML = "Password is required";
  } else {
    passwordError.innerHTML = "";
  }
}

function handleInput(elem) {
  // if(elem.id === "email") {
  //   credentials.email = elem.value
  // } else if(elem.id === "password") {
  //   credentials.password = elem.value
  // }
  credentials = {
    ...credentials,
    [elem.id]: elem.value,
  };
}

function handleEmailError(elem) {
  let regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
  let matchedEmail = regex.test(elem.value);
  if (!matchedEmail) {
    emailError.innerHTML = "Please enter valid email";
  } else {
    emailError.innerHTML = "";
  }
}
